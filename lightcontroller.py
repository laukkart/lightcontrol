import sys
import time
import serial
import queue
import threading

class LightController:
    
    usleep = lambda z,x: time.sleep(x/1000000.0)
    debug = lambda z,msg: print(__class__.__name__ + ": " + msg)

    limit_off = 128
    limit_full = 254
    
    def __init__(self, com, count, level, sleep=100000, debug=False):
        '''
        Constructor for LightController light control - the lights have ID 1,2,.. and use powers of 128-254

        NOTE: this class is not thread safe, and should be only used by one thread

        Parameters:
        com: the com port the controller is attached to
        count: the amount of lights, indexing is assumed to be continuos starting from value 1 up to count
        level: the intial level the lights are set to, levels are from 0 - 100% as integer, intesity is logarithmic
        sleep: defaults to 100000us, sets the sleep time between two consequetive command rounds
        debug: defaults to False, when turned True, module will print debug information
        '''
        if not debug:
            # there's no need for z to eat out the self here for some reason
            self.debug = lambda msg: None
            
        self.debug("Initializing")
        
        self._com = com
        self._count = count
        self._sleep = sleep
        
        # this initializes a list of levels
        self._lights = [level]*count
        
        self._queues = []
        for i in range(count):
            self._queues.append(queue.Queue())
            
        # start the worker thread, notice NOT to put () on the target - that would call the function
        self._worker = threading.Thread(target=self.serialworker, daemon=True)
        self._run = True
        self._worker.start()
        
        self.debug("Setting lamps to initial intensity")
        
        for idx, lvl in enumerate(self._lights):
            self.set_level(idx+1, lvl)

    def __del__(self):
        self.stopworker()

    def serialworker(self):
        '''
        serialworker takes care of serializing the commands to the control device
        The commands are delievered for each lamp ID in round robin fashion
        After each round robin round, a delay of set sleep is taken (default=100000)
        '''
        ser = serial.Serial(self._com)
        self.debug("Starting the serial worker at " + str(self._com))
        while self._run:
            for id, q in enumerate(self._queues):
                if not q.empty():
                    # get() or get_nowait() will throw exception Empty, if empty
                    # should not happen here, because this is only place where queues are read
                    cmd = q.get_nowait()
                    self.debug("Writing to lamp id: " + str(id+1) + " power: " + str(cmd))
                    ser.write(bytes([id+1]))
                    ser.write(bytes([cmd]))
                    ser.flush()
                    q.task_done()
            self.usleep(self._sleep)
        ser.close()
        self.debug("Worker thread exiting")

    def stopworker(self):
        '''
        stopworker stops the worker in controlled fashion.
        Useful to ensure that the serial line is released in closing.
        To rerun, one must create a new object of the class!
        '''
        self._run = False
        self._worker.join()


    def status(self):
        '''
        status returns False if there is on going adjustment. Otherwise return True
        This function can be used to ensure that the set smooth adjustments have been completed
        before sending further adjustments (e.g. to measure the ambient light after change)
        '''
        for q in self._queues:
            if not q.empty():
                return False
        return True
        
    def set_sleep(self, sleep=100000):
        '''
        set_sleep allows changing the sleep time taken in between command sending rounds
        fast sleep time or no sleep at all (sleep=0) will result fastest reaction, but
        also fasten up the smooth operations, and may congest the communication channel to lights

        Parameters:
        sleep: the time in us that is slept in between rounds
        '''
        self._sleep = sleep
    
    def set_level(self, id, level):
        '''
        set_level sets the level of lamp id

        Parameters:
        id: the lamp id, starting from 1
        level: the level of 0..100 the lamp is set to 
        '''
        self._queues[id-1].put(self.calc_real_level(level))
        self._lights[id-1] = level
            
    def get_level(self, id):
        '''
        get_level returns the currently set level for the lamp id.

        NOTE: the level may be different from the actual level on the lamp, if the commands
        have not yet been send to the lamps.

        Parameters:
        id: the lamp id, starting from 1, which level is returned
        '''
        return self._lights[id-1]

    def poll_on_going_commands(self, id):
        '''
        poll_on_going_commands returns True, if there is commands in the queue
        waiting to be sent to tha lamp. This can be used to check if the smooth
        operations have completed or not (assuming only one user thread)

        Parameters:
        id: the lamp id, which command queue is examined
        '''
        return not self._queues[id-1].empty()

    def get_level_smooth_duration(self, id, level):
        '''
        get_level_smooth_duration returns the time it will take to change level
        smoothly from current level to the desired level

        Parameters:
        id: the lamp id, which level is looked for
        level: the desired level in which the lamp is going to be changed
        '''
        level_past = self.calc_real_level(self._lights[id-1])
        level_target = self.calc_real_level(level)
        delta = abs(level_target - level_past)
        return int(delta*self._sleep/1000000)
        
    def set_level_smooth(self, id, level):
        '''
        set_level_smooth sets level smoothly, the time in between level increments
        is set by the sleep (default=100000us)

        NOTE: this function returns almost immediately, and after returning, the lamp
        level is set to the given level. However, the actual level change may still be on going
        '''
        inc = 1

        if level == self._lights[id-1]:
            return
        
        level_past = self.calc_real_level(self._lights[id-1])
        level_target = self.calc_real_level(level)
        
        if level_target < level_past:
            inc = -inc

        # this is kinda hack to stop stepping between 0..limit_off
        if level_past == 0:
            level_past = self.limit_off

        steps = abs(level_target - level_past)
        self.debug("Set level on id " + str(id) + " smooth to target level " + str(level_target) + " from level " + str(level_past) + " results to " + str(steps) + " steps")
        
        while(steps > 0):
            level_past += inc
            
            # this is kinda hack to stop stepping between 0..limit_off
            if level_past < self.limit_off:
                level_past = 0

            self._queues[id-1].put(level_past)

            # this is kinda hack to stop stepping between 0..limit_off
            if level_past < self.limit_off:
                break

            steps -= 1

        self._lights[id-1] = level
        self.debug("Set level smooth completed at target level " + str(level_target))

    def synchronize(self, level):
        '''
        synchronize synchronizes all lamps to the same level
        '''
        for idx,lvl in enumerate(self._lights):
            self.set_level(idx+1, level)
            self._lights[idx] = level

        
    def synchronize_smooth(self, level):
        '''
        synchronize_smooth synchronizes all lamps to the same level smoothly
        '''
        level_target = self.calc_real_level(level)
        
        levels = self._lights
        for idx,lvl in enumerate(self._lights):
            levels[idx] = self.calc_real_level(lvl)

        while(1 > 0):
            stop = True
            for idx,lvl in enumerate(levels):
                if lvl > level_target:
                    if lvl - 1 < self.limit_off:
                        levels[idx] = 0
                    else:
                        levels[idx] = lvl - 1
                    stop = False
                elif lvl < level_target:
                    if lvl + 1 > self.limit_full:
                        levels[idx] = self.limit_full
                    else:
                        levels[idx] = lvl + 1
                    stop = False
                else:
                    # we are at target
                    continue
                self._queues[idx].put(levels[idx])
            if stop:
                break
            
        # TODO: this could be on the first for loop
        for idx,lvl in enumerate(self._lights):
            self._lights[idx] = level

        
    def calc_real_level(self, level):
        '''
        calc_real_level is for internal use only. It calculates the real lamp level from 0..100
        '''
        if level == 0:
            return 0
        else:
            delta = self.limit_full - self.limit_off
            delta = int(delta*(level/100))
            # sanity check, this should not really happen:
            if self.limit_off + delta > self.limit_full:
                return self.limit_full
            return self.limit_off + delta

        
