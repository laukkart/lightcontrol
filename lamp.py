import time
import opencv_filter

class Lamp:

    debug = lambda z,msg: print(__class__.__name__ + ": " + msg)
    
    x_start = 0
    x_end = 0
    y_start = 0
    y_end = 0
     
    def __init__(self, id, coords, control, debug=False):
        self._id = id
        self._coords = coords
        self.x_start = coords[0]
        self.x_end = coords[0] + coords[2]
        self.y_start = coords[1]
        self.y_end = coords[0] + coords[3]
        self._control = control
        self._tick = 0
        self._power = 0
        self._finetuned = False
        self._humans_detected_cnt = 0
        self._last_human_detection = 0.0

        self._MAX_POWER = 100
	# can be overriden by finetuning
        self._DEFAULT_POWER = 70
        self._LOW_POWER = 30
        self._MIN_POWER = 20
        self._BLACKOUT_POWER = 0
        self._FINETUNE_INC = 5
        self._RESET_TIMEOUT = 60.00
        self._BLACKOUT_TIMEOUT = 120.00

        self._humans_filter = opencv_filter.OpenCVFilter(20)


    def startup(self):
        self.debug("Startup")
        self._power = self._LOW_POWER
        self._control.set_level_smooth(self._id, self._power)
        self._finetuned = False

    def shutdown(self):
        self.debug("Shutdown")
        self._power = self._BLACKOUT_POWER
        self._control.set_level_smooth(self._id, self._power)
        self._finetuned = False

    def get_status(self):
        if self._power == self._BLACKOUT_POWER:
           return "OFF"
        elif self._power == self._LOW_POWER:
           return "LOW"
        else:
           return "NORMAL"

    def get_power(self):
        return self._power

    def finetune(self, power):
        if self._power <= self._MAX_POWER and self._power > self._MIN_POWER:
            self._power += power
            self._control.set_level(self._id, self._power)
            self._finetuned = True

    def reset_finetune(self):
        self._power = self._DEFAULT_POWER
        self._control.set_level(self._id, self._power)
        self._finetuned = False
    
    def humans_detected(self, humans):
        self._humans_filter.add_detections(humans)
        if self._humans_filter.get_current_count() > 1:
            self.debug("Humans detected for lamp " + str(self._id))
            if not self._finetuned and self._power != self._DEFAULT_POWER:
                self._power = self._DEFAULT_POWER
                self._control.set_level_smooth(self._id, self._power)
                # lets give a bit of head start for human detections during our first detection
                #self._humans_detected_cnt += 50
                self._last_human_detection = time.time()
            else:
                self._last_human_detection = time.time()
                #self._humans_detected_cnt += 10
            self._humans_filter.purge()

    def humans_detected_no_filter(self):
        self.debug("Humans detected for lamp " + str(self._id))
        if not self._finetuned and self._power != self._DEFAULT_POWER:
            self._power = self._DEFAULT_POWER
            self._control.set_level_smooth(self._id, self._power)
            # lets give a bit of head start for human detections during our first detection
            #self._humans_detected_cnt += 50
            self._last_human_detection = time.time()
        else:
            self._last_human_detection = time.time()
            #self._humans_detected_cnt += 10


    def humans_detected_no_power_change(self):
        self._last_human_detection = time.time()

    def no_humans(self):
        if self._power != self._LOW_POWER and self._power != self._BLACKOUT_POWER and self._last_human_detection + self._RESET_TIMEOUT < time.time():
            # over 15min from previous detection, reset
            self._power = self._LOW_POWER
            self._finetuned = False
            self.debug("Resetting lamp " + str(self._id) + " due inactivity")
            self._control.set_level_smooth(self._id, self._power)
            
        if self._power == self._LOW_POWER and self._last_human_detection + self._BLACKOUT_TIMEOUT < time.time():
            self._power = self._BLACKOUT_POWER
            self._finetuned = False
            self.debug("Blackout lamp " + str(self._id) + " due inactivity")
            self._control.set_level_smooth(self._id, self._power)

            
    def gesture_dec(self):
        if not (not self._finetuned and self._power == self._LOW_POWER):
            self.debug("Decreasing lamp " + str(self._id))
            if self._power > 0:
                self._power -= self._FINETUNE_INC
            if self._power < self._MIN_POWER:
                self._power = self._MIN_POWER
            self._finetuned = True
            self._control.set_level(self._id, self._power)

    def gesture_inc(self):
        if not (not self._finetuned and self._power == self._LOW_POWER):
            self.debug("Increasing lamp " + str(self._id))
            self._power += self._FINETUNE_INC
            if self._power > self._MAX_POWER:
                self._power = self._MAX_POWER
            self._finetuned = True
            self._control.set_level(self._id, self._power)
            
    def detect_humans(self):
        self._tick += 1
        self._humans_filter.tick()
        if self._tick % 4 == 0:
            return True
        return False

    def detect_gestures(self):
        self._humans_filter.tick()
        if self._tick % 4 == 0:
            #self._tick += 1
            return False
        #self._tick += 1
        return True

