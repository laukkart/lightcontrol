import numpy as np
import cv2
import time

import socket

import lightcontroller as lightcontroller

import lamp as lamp



usleep = lambda x: time.sleep(x/1000000.0)
debug = lambda msg: print("Main: " + msg)



def numpy2tuple(coords):
    x = coords.item(0)
    y = coords.item(1)
    w = coords.item(2)
    h = coords.item(3)
    return [x,y,w,h]

def tuple2numpy(t):
    return np.array(t)


    

if __name__ == "__main__":
    # These cascades are available at https://github.com/OAID/CVGesture
    fist = cv2.CascadeClassifier('haarcascades/fist_v3.xml')
    palm = cv2.CascadeClassifier('haarcascades/palm_v4.xml')
    #human = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_frontalface_default.xml')
    human = cv2.CascadeClassifier(cv2.data.haarcascades + 'haarcascade_upperbody.xml')
    vres = 720 #540
    hres = 1280 #960

  
    vcap = cv2.VideoCapture(0)
    # set width
    vcap.set(3,hres)
    # height
    vcap.set(4,vres)

    control = lightcontroller.LightController("COM8", 2, 0, debug=False)

    lamps = []
    lamps.append(lamp.Lamp(1, [0,0,int(hres/2),vres-140], control))
    lamps.append(lamp.Lamp(2, [int(hres/2),0,int(hres/2),vres-140], control))

    # TODO: Do calibration
   
    # wait for pir  
    while True:
        # a workaround to ensure that socket is always empty, when we start waiting for PIR
        sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        server = ('', 10000)
        sock.bind(server)
        print("debug")
        data, addr = sock.recvfrom(1024)
        print("Received data " + str(data) + " from " + str(addr))
        if data.decode('utf-8') == "PIR ON":
            sock.close()
            for lamp in lamps:
                lamp.startup()
                lamp.humans_detected_no_power_change()

            loop = True
            while loop:
                # detect humans and gestures

                # capture image
                ret, img = vcap.read()
                #img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
                #img = cv2.equalizeHist(img)
                #img = cv2.flip(img, -1)

                lamp = lamps[1]

                area = img[lamp.y_start:lamp.y_end, lamp.x_start:lamp.x_end]
                #cv2.imshow('video',area)
                #while True:
                #    cv2.waitKey(10)

                if lamp.detect_humans():
                    #debug("Detecting humans")
                    humans, hrejects, hweights = human.detectMultiScale3(area, scaleFactor=1.2, minNeighbors=6, minSize=(75, 75), maxSize=(300,300), outputRejectLevels=True)
                    if len(humans) > 0 and hweights[0] > 0.0:
                        debug("Humans found at " + str(humans))
                        debug("Humans weights " + str(hweights))
                        #lamp.humans_detected(humans)
                        lamp.humans_detected_no_filter()
                    else:
                        lamp.no_humans()
                        if lamp.get_status() == "OFF":
                            # check other lamps, and stop looping if all are OFF
                            loop = False
                            for l in lamps:
                                if l.get_status() != "OFF":
                                    loop = True
                                    break

                if lamp.detect_gestures() and lamp.get_status() == "NORMAL":
                    #debug("Detecting gestures")
                    fists, frejects, fweights = fist.detectMultiScale3(area, scaleFactor=1.2, minNeighbors=6, minSize=(20, 20), maxSize=(100,100), outputRejectLevels=True)
                    palms, prejects, pweights = palm.detectMultiScale3(area, scaleFactor=1.2, minNeighbors=2, minSize=(20, 20), maxSize=(100,100), outputRejectLevels=True)
                    if len(fists) > 0 and fweights[0] > 0.1:
                        debug("Fists found at " + str(fists))
                        debug("Fists weights " + str(fweights))
                        lamp.humans_detected_no_power_change()
                        lamp.gesture_dec()

                    if len(palms) > 0 and pweights[0] > 0.1:
                        debug("Palms found at " + str(palms))
                        debug("Palms weights " + str(pweights))
                        lamp.humans_detected_no_power_change()
                        lamp.gesture_inc()


                # move this lamp to wait its turn
                lamps = lamps[1:] + [lamps[0]]

                # check if all lamps off
                
                cv2.imshow('video',img)
                cv2.waitKey(10)
                #usleep(100)


    vcap.release()
    cv2.destroyAllWindows()
