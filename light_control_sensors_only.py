import lightcontroller_v2 as lightcontroller
import time

import openPIR
import APDS9301
import RFD77402

import serial
import smbus2

import lamp as lamp_mod

import threading
import queue

CONFIG_APDS9301_ADDR = 0x39
CONFIG_RASPI_I2C_BUS = 1

gestures = queue.Queue()

def serialworker():
    print("Serial worker running")
    ser = serial.Serial("/dev/ttyUSB0", 115200)
    while True:
        s = ser.read()
        if s == (0xFC).to_bytes(1, byteorder='big'):
            s = ser.read()
            print("Gesture " + str(s))
            if s == (0x01).to_bytes(1, byteorder='big'):
                print("Gesture inc")
                #lamp.gesture_inc()
                gestures.put("INC")
            if s == (0x02).to_bytes(1, byteorder='big'):
                print("Gesture dec")
                gestures.put("DEC")
                #lamp.gesture_dec()

if __name__ == "__main__":
    # workings:
    # 1. catch PIR -> low power
    # 2. detect laser -> increase power
    # 3. finetune based on measurement
    # 4. keep detecting PIR and laser
    # 5. check gestures for further finetuning
    # 6. if PIR and laser do not detect -> low power
    # 7. after long time of no detection -> off

    control = lightcontroller.LightController("/dev/ttyUSB1", 2, 0, debug=True)

    lamp = lamp_mod.Lamp(1, [0,0,0,0], control, debug=True)
    #lamp.shutdown()

    bus1 = smbus2.SMBus(CONFIG_RASPI_I2C_BUS)
    #ser = serial.Serial("/dev/ttyUSB0", 115200)
    
    PIR = openPIR.PIR()
    luminosity = APDS9301.APDS9301(bus1, CONFIG_APDS9301_ADDR)
    laser = RFD77402.RFD77402(bus1)

    thrd = threading.Thread(target=serialworker, daemon=True)
    thrd.start()
    lamp.shutdown()


    laser_calib, err = laser.read(bus1)
    laser_calib, err = laser.read(bus1)
    print("LASER calibrated at distance " + str(laser_calib))
    calibrated = False

    while True:
        PIR_detection = PIR.poll()
        laser_detection = laser.detect_obstacle(bus1)
        
        if PIR_detection:
            print("PIR detection")
            if lamp.get_power() == 0:
                print("Lamp startup")
                lamp.startup()
            else:
                lamp.humans_detected_no_power_change()

        if laser_detection is not None and laser_detection < (laser_calib - 10):
            print("LASER detection at " + str(laser_detection))
            if lamp.get_power() == 0:
                print("Lamp startup")
                lamp.startup()
                lamp.humans_detected_no_filter()
            else:
                lamp.humans_detected_no_filter()


        if (not PIR_detection) and (laser_detection is None):
            print("no detection")
            lamp.no_humans()
                
        if lamp.get_power() != 0 and control.status() and not calibrated:
            lum = luminosity.read(bus1)
            if lum < 30:
                pass
                print("Finetune inc, lum " + str(lum))
                lamp.finetune(5)
            if lum > 60:
                pass
                print("Finetune dec, lum " + str(lum))
                lamp.finetune(-5)
            calibrated = True
                

        if not gestures.empty():
            ges = gestures.get_nowait()
            if ges == "INC":
                lamp.gesture_inc()
            if ges == "DEC":
                lamp.gesture_dec()
            gestures.task_done()
                
        
