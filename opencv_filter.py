import time
import cv2


class OpenCVFilter:
    # a simple class to filter and group opencv haarcascade detections within a time window
    debug = lambda z,msg: print(__class__.__name__ + ": " + msg)
    
    def __init__(self, threshold=10, debug=False):
        self._additions = []
        self._previous_addition = 0
        self._times = 0
        self._tick = 0
        self._threshold = threshold

        if not debug:
            # turn off debugging
            # there's no need for z to eat out the self here for some reason
            self.debug = lambda msg: None


    def add_detection(self, coords):
        if self._previous_addition > 0:
            if self._tick < self._previous_addition + self._threshold:
                # previous addition was in recent history, add
                self._previous_addition = self._tick
                self._additions += [coords]
                self._times += 1
            else:
                # long time from previous finding, purge, and add as first
                self.debug("TO")
                self.purge()
                self._previous_addition = self._tick
                self._additions += [coords]
                self._times += 1
        else:
            self.purge()
            self._previous_addition = self._tick
            self._additions += [coords]
            self._times += 1

        return len(self._additions)

    def add_detections(self, detections):
        for coords in detections:
            self.add_detection(coords)

        return len(self._additions)

    def get_detections_grouped(self):
        thresh = 1
        rectList, weights = cv2.groupRectangles(self._additions, thresh, eps=0.5)
        return rectList

    def get_current_count(self):
        return self._times
    
    def purge(self):
        self._additions = []
        self._previous_addition = 0
        self._times = 0
    
    def tick(self):
        self._tick += 1
