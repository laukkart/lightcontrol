import openPIR as PIR
import socket
import time

usleep = lambda x: time.sleep(x/1000000.0)

if __name__ == "__main__":
    pir = PIR.PIR()

    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    server = ('localhost', 10000)

    alarm = False
    
    while True:
        if not alarm and pir.poll():
            sock.sendto("PIR ON".encode('utf-8'), server)
            alarm = True
            print("Alarm ON")
        elif alarm and not pir.poll():
            alarm = False
            print("Alarm off")

        usleep(5000)
            
    
